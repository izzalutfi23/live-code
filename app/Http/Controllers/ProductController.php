<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(){
        return view('index');
    }

    public function get(Request $request){
        if($request){
            if($request->keyword){
                $searchTerm = $request->keyword;
                $products = Product::whereRaw('LOWER(name) like ?', ['%' . strtolower($searchTerm) . '%'])->paginate(10);
            }
            elseif($request->price){
                $minPrice = 0;
                $maxPrice = $request->price;
                $products = Product::whereBetween('price', [$minPrice, $maxPrice])->paginate(10);
            }
            else{
                $products = Product::paginate(10);
            }
        }
        else{
            $products = Product::paginate(10);
        }

        foreach($products as $product){
            $product->image = url('products/'.$product->image);
        }

        return response()->json([
            'status' => 'ok',
            'message' => 'Data product',
            'data' => $products
        ], 200);
    }

    public function store(Request $request){
        $rules = [
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'stock' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        else{
            $file = $request->file('image');
            $fileName = date('Y-m-d H:i:s').$file->getClientOriginalName();

            $file->move('products',$fileName);

            product::create([
                'name' => $request->name,
                'price' => $request->price,
                'description' => $request->description,
                'stock' => $request->stock,
                'image' => $fileName
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Product created!'
            ], 201);
        }
    }

    public function update(Request $request){
        $rules = [
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'stock' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }
        else{
            if($request->image){
                $file = $request->file('image');
                $fileName = date('Y-m-d H:i:s').$file->getClientOriginalName();

                $file->move('products',$fileName);

                product::whereId($id)->update([
                    'name' => $request->name,
                    'price' => $request->price,
                    'description' => $request->description,
                    'stock' => $request->stock,
                    'image' => $fileName
                ]);
            }
            else{
                product::whereId($id)->update([
                    'name' => $request->name,
                    'price' => $request->price,
                    'description' => $request->description,
                    'stock' => $request->stock
                ]);
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Product updated!'
            ], 200);
        }
    }

    public function destroy($id){
        Product::whereId($id)->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Product deleted'
        ], 200);
    }
}
