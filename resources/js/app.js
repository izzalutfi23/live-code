require('./bootstrap');

window.Vue = require('vue').default;

import { Form } from 'vform'
import {
    Button,
    HasError,
    AlertError,
    AlertErrors,
    AlertSuccess
  } from 'vform/src/components/bootstrap5'
Vue.component(Button.name, Button)
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component(AlertErrors.name, AlertErrors)
Vue.component(AlertSuccess.name, AlertSuccess)
window.Form = Form;

import swal from 'sweetalert2';
window.Swal = swal;

import axios from 'axios';
window.axios = axios;

Vue.directive('select2', {
    inserted(el) {
        $(el).on('select2:select', (e) => {
            const event = new Event('change', {  target: e.target });
            el.dispatchEvent(event);
        });

        $(el).on('select2:unselect', (e) => {
            const event = new Event('change', { target: e.target})
            el.dispatchEvent(event)
        })
    },
});

// Sweet Alert
import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);